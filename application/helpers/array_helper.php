<?php

/**
 * @Author: Atish Agrawal
 * @Date:   2018-02-08 16:40:40
 * @Last Modified by:   Atish Agrawal
 * @Last Modified time: 2018-02-08 16:51:10
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!function_exists('encrypt_array')) {
    function encrypt_array($plainArray = '')
    {

        if ($plainArray !== '' && !empty($plainArray)) {
            $encryptedArray = array();

            $ci = &get_instance();
            $ci->load->library('encryption');

            foreach ($plainArray as $key => $value) {
                $encryptedArray[$key] = $ci->encryption->encrypt($value);
            }

            return $encryptedArray;
        }
    }
}

if (!function_exists('decrypt_array')) {
    function decrypt_array($encryptedArray = '')
    {
        if ($encryptedArray !== '' && !empty($encryptedArray)) {
            $plainArray = array();

            $ci = &get_instance();
            $ci->load->library('encryption');

            foreach ($encryptedArray as $key => $value) {
                $plainArray[$key] = $ci->encryption->decrypt($value);
            }

            return $plainArray;
        }
    }
}
