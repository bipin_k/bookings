<?php

/**
 * @Author: Atish Agrawal
 * @Date:   2017-08-24 18:07:09
 * @Last Modified by:   Atish Agrawal
 * @Last Modified time: 2018-10-16 21:11:59
 */

class Usermodel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    // Checking for existing user
    public function login($email, $password)
    {

        $this->db->where('email', $email);
        $this->db->where('pswd', $password);
        $que = $this->db->get('login');
        if ($que->num_rows() > 0) {
            return $que->result_array();
        } else {
            return false;
        }
    }

    public function checkEmail($email)
    {
        $this->db->where('email', $email);
        $que = $this->db->get('login');
        if ($que->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
