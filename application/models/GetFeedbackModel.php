<?php
class GetFeedbackModel extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getAllData()
	{
		$sql = 'SELECT * FROM feedback;';
		$query = $this->db->query($sql);
		return $query->result();
	}
}
?>