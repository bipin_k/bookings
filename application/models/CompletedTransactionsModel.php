<?php
class CompletedTransactionsModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getCompletedTransactions()
    {
    	$sql="select * from confirmedbookings cb left join bookingtransactions bt on cb.id = bt.id where hascompleted = true order by cb.checkoutdate desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getCompletedTransactionsForId($id)
    {
    	$sql="select * from confirmedbookings cb left join bookingtransactions bt on cb.id = bt.id where cb.id='$id' and hascompleted = true";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getAllPriceOfRoom()
    {
        $sql="select * from roomprice";
        $query = $this->db->query($sql);
        return $query->result();
    }


}