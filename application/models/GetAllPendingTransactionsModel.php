<?php
class GetAllPendingTransactionsModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getAllPendingTransactionData()
    {
    	$sql= "select * from confirmedbookings, bookingtransactions where confirmedbookings.id=bookingtransactions.id and hascompleted = false and checkindate < curdate() order by confirmedbookings.checkindate desc";
    	$query = $this->db->query($sql);
		return $query->result();
    }

    public function getDataForTransactionId($id)
    {
        $sql="select * from confirmedbookings, bookingtransactions where confirmedbookings.id=$id and confirmedbookings.id=bookingtransactions.id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getRoomPrice($id)
    {
        $sql= "select bookedrooms, checkindate, checkintime from confirmedbookings where id=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getRoomPriceWithCheckoutData($id)
    {
        $sql= "select bookedrooms, checkindate, checkintime, checkoutdate, checkouttime from confirmedbookings where id='$id'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getPriceOfEachRoom($roomId)
    {
        $sql= "select price from roomprice where id='$roomId'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function updatediscount($id, $discount, $pending)
    {
        $sql= "update bookingtransactions set discount= $discount, pending= $pending where id='$id'";
        $query = $this->db->query($sql);
        return $this->db->affected_rows();
    }

    public function updateCheckoutData($id, $date, $time, $count)
    {
        $sql= "update confirmedbookings set checkoutdate='$date', checkouttime='$time', dayscount=$count where id='$id'";
        $query = $this->db->query($sql);
    }

    public function finalSubmitForCheckout($id)
    {
        $sql= "select * from confirmedbookings cb left join bookingtransactions bt on cb.id = bt.id where bt.id = '$id'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function checkoutRoomForUserModel($id, $durationPeriod)
    {
        $sql= "update confirmedbookings set duration= '$durationPeriod', hascompleted=true where id='$id'";
        $query = $this->db->query($sql);
        return $this->db->affected_rows();
    }

    public function freeRoom($checkindate, $checkoutdate, $room)
    {
        $sql= "update bookingstatus set $room=null where date between '$checkindate' and '$checkoutdate'";
        $query = $this->db->query($sql);
    }

    public function updateTaxForId($id, $amount)
    {
        $sql= "update bookingtransactions set tax=$amount where id='$id'";
        $query = $this->db->query($sql);
    }


}