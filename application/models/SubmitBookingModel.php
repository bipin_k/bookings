<?php
class SubmitBookingModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function submitBookingTransaction($bill, $checkin, $checkout, $rooms, $name, $email, $mobile, $dependents, $documentType, $otherdoc, $documentNumber, $address, $purpose, $checkintime, $checkouttime, $bookingamount)
    {
        //query to update booking transaction table in confirmedbookings

        $uniqueId= round(microtime(true) * 1000);
        $sql= "insert into confirmedbookings(billid, id, checkindate, checkoutdate, bookedrooms, name, email, mobile, dependents, documentType, otherdoc, documentNumber, address, purpose, checkintime, checkouttime, bookingamount, initialCheckoutDate) values($bill, '$uniqueId', '$checkin', '$checkout', '$rooms', '$name', '$email', '$mobile', '$dependents', '$documentType', '$otherdoc', '$documentNumber', '$address', '$purpose', '$checkintime', '$checkouttime', '$bookingamount', '$checkout')";
        $this->db->query($sql);

        //query to update booking status of rooms in table bookingstatus for all rooms booked
        
        $str=$_SESSION['rooms'];
        $arr=explode(",",$str);
        $count= count($arr);
        $query='update bookingstatus set ';
        for($i= 0; $i< $count; $i++)
        {
            $query= $query.$arr[$i].'= true, ';
        }
        $query=rtrim($query,", ");
        $query= $query.' where date between '.'\''.$_SESSION['checkin'].'\''.' and '.'\''.$_SESSION['checkout'].'\''.';';
        print_r($query);
        $this->db->query($query);

        //query to insert advance amount in bookingtransactions

        $transactionquery= "insert into bookingtransactions(id, advance) values('$uniqueId', '$bookingamount')";
        $this->db->query($transactionquery);        


        return ($this->db->affected_rows() != 1) ? false : true;        
    }

    




    // public function getBookingStatus($checkin, $checkout)
    // {
    //     $query = $this->db->query("select * from bookingstatus where date between '" . $checkin . "' and '" . $checkout . "'");
    //     if ($query->num_rows() > 0) {
    //         $row = $query->row_array();
    //         return $query->result();
    //     } else {
    //         return array('message' => "No data found!");
    //         // return array('message'=>0, 'msg' => "No data");
    //     }
    // }
}
