<?php
class HotelBookingStatusModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getBookingStatus($checkin, $checkout)
    {
        $query = $this->db->query("select * from bookingstatus where date between '" . $checkin . "' and '" . $checkout . "'");
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $query->result();
        } else 
        {
            return array('message' => "No data found!");
        }
    }

    public function checkIfDateIsAvailable($checkDate)
    {
        $sql= "select exists (select * from bookingstatus where date='$checkDate') as status";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function insertDate($currentDate)
    {
        $sql= "insert into bookingstatus(date) values('$currentDate')";
        $query = $this->db->query($sql);
        // return $query->row();
    }

    public function getAdvanceBookingsModel()
    {
        $sql= "select * from confirmedbookings, bookingtransactions where confirmedbookings.id=bookingtransactions.id and hascompleted = false and checkindate > curdate() order by confirmedbookings.checkindate asc";
        $query = $this->db->query($sql);
        return $query->result();
    }

public function getBillNumberModel()
    {
        $this->db->query("update billno set id=id+1");
        $query = $this->db->query('select id from billno');
        $row = $query->row();
        return $row->id;
    }
}
