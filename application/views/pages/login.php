<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- Sweet Alert -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/2.9.0/jquery.serializejson.min.js"></script>
	<!-- Sweet Alert -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<script>
		// window.onscroll = function() {myFunction()};

		// var navbar = document.getElementById("navbar");
		// var sticky = navbar.offsetTop;

		// function myFunction() {
		// 	if (window.pageYOffset >= sticky) {
		// 		navbar.classList.add("sticky")
		// 	} else {
		// 		navbar.classList.remove("sticky");
		// 	}
		// }
	</script>
	<style>
	body {
		margin: 0;
		font-size: 18px;
	}

	.outer-wrapper {
		display: table;
		width: 100%;
		height: 100%;
	}

	.inner-wrapper {
		display:table-cell;
		vertical-align:middle;
		padding:15px;
	}
	.login-btn { position:fixed; top:15px; right:15px; }

	.header {
		font-size: 40px;
		background-color: #f1f1f1;
		padding: 5px;
		height: 90px;
		line-height: 90px;
		text-align: center;
	}

	#navbar {
		overflow: hidden;
		background-color: #333;
	}

	#navbar a {
		float: left;
		display: block;
		color: #f2f2f2;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		font-size: 17px;
	}

	#navbar a:hover {
		background-color: #ddd;
		color: black;
	}

	#navbar a.active {
		background-color: #4CAF50;
		color: white;
	}

	.content {
		padding: 16px;
	}

	.sticky {
		position: fixed;
		top: 0;
		width: 100%;
	}

	.center_div{
		margin: 0 auto;
		width:80% /* value of your choice which suits your alignment */
	}

	.sticky + .content {
		padding-top: 60px;
	}


	.room-number{
		margin: 5px;
		background-color: grey;
		width: 100px;
		height: 100px;
		text-align: center;
		vertical-align: middle;
		display: table;
	}
	.room-number p{
		padding: 5px;
		display: table-cell;
		text-align: center;
		vertical-align: middle;
	}
	.room-number-booked{
		margin: 5px;
		background-color: red;
		width: 100px;
		height: 100px;
		text-align: center;
		vertical-align: middle;
		display: table;
	}
	.room-number-booked p{
		padding: 5px;
		display: table-cell;
		text-align: center;
		vertical-align: middle;
	}

</style>
</head>
<body>

	<div class="header">
		Welcome To Hotel Shivaay Residency!
	</div>




	<section id="loginform" class="outer-wrapper">
		<div class="inner-wrapper">
			<div class="container">

				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<!-- <h2>Please login to continue!<br><br></h2><br> -->
						<?php echo form_open('LoginController/loginSuccess', 'class="form-horizontal" id="feedback"'); ?>
						<div class="form-group">
							<label for="email">Email address</label>
							<input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" class="form-control" id="password" name="password" placeholder="Password">
						</div>

						<button type="submit" class="btn btn-success">Submit</button>
						<button type="reset" class="btn btn-default"> Reset</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>



<!-- Code for SweetAlert.js -->
<?php if ($this->session->flashdata('msg')) {
    ?>
	<script>
		<?php
switch ($this->session->flashdata('msg')) {
        case "invalidentries":
            ?>
			swal({   title: "Warning!",   text: "Incorrect credentials.",   timer: 3000,   showConfirmButton: true, closeOnConfirm: true, type:"warning" });
			<?php
break;

        case "accountNotActivated":
            ?>
			swal({   title: "Account Not Verified Yet!",   text: "Your account is not activated yet. A verification code was sent to your registered mobile number. Please verify your identity to activate the account.", showConfirmButton: true, closeOnConfirm: true, type:"info" });
			<?php
break;
    }
    ?>
	</script>
<?php }?>
