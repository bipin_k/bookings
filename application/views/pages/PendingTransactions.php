<div id="navbar">
	<a href="<?php echo site_url('CheckAvailabilityController'); ?>">Check Availability</a>
	<a class="active" href="<?php echo site_url('GetAllPendingTransactionsController'); ?>">Confirmed Bookings</a>
	<a href="<?php echo site_url('CompletedTransactionsController'); ?>">Bill</a>
	<a href="<?php echo site_url('CheckAvailabilityController/getAdvanceBookings'); ?>">Advance Bookings</a>
	<a href="<?php echo site_url('LoginController/logout'); ?>">Logout</a>
</div>

<section>
	<br>
	<div class="col-sm-12">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Transaction ID</th>
					<th>Name</th>
					<th>Checkin</th>
					<th>Rooms</th>
					<th>Checkout</th>
					<th>View</th>
					<th>Submit</th>
				</tr>
			</thead>
			<tbody>
				<?php
foreach ($data as $item) {
    print_r('<tr><td>');
    print_r($item->id);
    print_r('</td>');

    print_r('<td>');
    print_r($item->name);
    print_r('</td>');

    print_r('<td>');
    print_r(date("d/m/Y", strtotime($item->checkindate)) . ', ' . $item->checkintime);
    print_r('</td>');

    print_r('<td>');
    print_r(str_replace('_', ' ', strtoupper($item->bookedrooms)));
    print_r('</td>');

    print_r('<td>');
    print_r(date("d/m/Y", strtotime($item->checkoutdate)) . ', ' . $item->checkouttime);
    print_r('</td>');

    print_r('<td>');
    print_r('<button type="button" id="clearButton" name="clearButton" class="btn btn-info" value="');
    print_r($item->id);
    print_r('" data-toggle="modal" data-target="#myModal">View</button>');
    print_r('</td>');

    print_r('<td>');
    print_r('<button type="submit" class="btn btn-success" value="');
    print_r($item->id);
    print_r('" data-dismiss="modal" id="checkout-button" name="checkout-button" ');
    // print_r(' onclick="location.href=\'getCheckoutData?id=\' " ');
    print_r(' onclick="location.href=\'GetAllPendingTransactionsController/getCheckoutData?id=');
    print_r($item->id);
    print_r('\' " ');

    // \' " ');
    print_r('>Checkout</button>');
    print_r('</td>');

    print_r('</tr>');
}
?>

				</tbody>
		</table>


		<div class="modal" id="myModal" name="myModal">
			<div class="modal-dialog">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<!-- <h4 class="modal-title">Submit</h4> -->
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<div class="modal-body">
						<table class="table table-striped" name="modalTable" id="modalTable">
							<!-- <thead>
								<tr>
									<th>Name</th>
									<th>Value</th>
								</tr>
							</thead> -->
							<tbody>

							</tbody>
						</table>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer" id="modal-footer" name="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">

	$('#myModal').on('hidden.bs.modal', function () {
		location.reload();
	});

	// $(document).on('click', '#checkout-button', function()
	// {
	// 	var me= $(this);
	// 	$.ajax({
			// url : '<?php #echo site_url('GetAllPendingTransactionsController/getTransactionId'); ?>',
	// 		type: 'GET',
	// 		data: {
	// 			id: me.val(),
	// 		},
	// 		success: function(response)
	// 		{

	// 		},
	// 		fail: function()
	// 		{
	// 			swal( 'Error', 'An error occured' , 'error');
	// 		}
	// 	});

	// });


	$(document).on('click', '#clearButton', function()
	{
		var me = $(this);

		$.ajax({
			url : '<?php echo site_url('GetAllPendingTransactionsController/getPriceOfRooms'); ?>',
			type: 'GET',
			success: function(response)
			{
				console.log(response);

			},
			fail: function()
			{
				swal( 'Error', 'An error occured' , 'error');
			}
		});

		$.ajax({
			url : '<?php echo site_url('GetAllPendingTransactionsController/getTransactionId'); ?>',
			type: 'GET',
			data: {
				id: me.val(),
			},
			success: function(response)
			{
				// console.log(response);
				$('<tr><td>Bill No.</td><td>'+response.id+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Name</td><td>'+response.name+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Email</td><td>'+response.email+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Mobile</td><td>'+response.mobile+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Dependents</td><td>'+response.dependents+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Document Type</td><td>'+response.documentType+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Other Document</td><td>'+response.otherdoc+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Document Number</td><td>'+response.documentNumber+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Address</td><td>'+response.address+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Purpose</td><td>'+response.purpose+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Checkin</td><td>'+response.checkindate+', '+response.checkintime+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Checkout</td><td>'+response.checkoutdate+', '+response.checkouttime+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Advance Amount</td><td>'+response.bookingamount+'</td></tr>').appendTo($("#modalTable"));
				$('<tr><td>Booked Rooms</td><td>'+response.bookedrooms.toUpperCase().split('_').join(' ')+'</td></tr>').appendTo($("#modalTable"));
			},
			fail: function()
			{
				swal( 'Error', 'An error occured' , 'error');
			}
		});
	});
</script>
