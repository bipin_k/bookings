<div id="navbar">
		<a href="<?php echo site_url('CheckAvailabilityController'); ?>">Check Availability</a>
		<a class="active" href="<?php echo site_url('GetAllPendingTransactionsController'); ?>">Confirmed Bookings</a>
		<a href="<?php echo site_url('CompletedTransactionsController'); ?>">Bill</a>
		<a href="<?php echo site_url('CheckAvailabilityController/getAdvanceBookings'); ?>">Advance Bookings</a>
		<a href="<?php echo site_url('LoginController/logout'); ?>">Logout</a>
	</div>

<section>
	<div class="col-sm-2"></div>
	<div class="col-sm-8">

	<h2>Confirm Booking Now!</h2>
	<p>Please reconfirm following details prior to final submission!</p>
	<br>

	<table class="table table-striped">
		<!-- <thead>
			<tr>
				<th>Name</th>
				<th>Value</th>
			</tr>
		</thead> -->
		<tbody>
			<tr>
				<td>Booking Name</td>
				<td><?php echo ("{$_SESSION['name']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><?php echo ("{$_SESSION['email']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td>Mobile</td>
				<td><?php echo ("{$_SESSION['mobile']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td>No. of Dependents</td>
				<td><?php echo ("{$_SESSION['dependents']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td>Document Type</td>
				<td><?php echo ("{$_SESSION['documentType']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td>Other Document</td>
				<td><?php echo ("{$_SESSION['otherdoc']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td>Document Number</td>
				<td><?php echo ("{$_SESSION['documentNumber']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td>Purpose of Visit</td>
				<td><?php echo ("{$_SESSION['purpose']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td>Checkin Date</td>
				<td><?php echo ("{$_SESSION['checkin']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td>Checkin Time</td>
				<td><?php echo ("{$_SESSION['checkintime']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td>Checkout Date</td>
				<td><?php echo ("{$_SESSION['checkout']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td>Checkout Time</td>
				<td><?php echo ("{$_SESSION['checkouttime']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td>Address</td>
				<td><?php echo ("{$_SESSION['address']}" . "<br />"); ?></td>
			</tr>

			<?php
$finalroom = $_SESSION['rooms'];
$finalroom = strtoupper($finalroom);
$finalroom = str_replace('_', ' ', $finalroom);

?>

			<tr>
				<td>Rooms Booked</td>
				<td><?php echo $finalroom; ?></td>
			</tr>
			 <tr>
				<td>Booking Amount</td>
				<td><b>Rs. <?php echo ("{$_SESSION['bookingamount']}" . "/- only"); ?></b></td>
			</tr>
<!--			<tr>
				<td></td>
				<td><?php echo ("{$_SESSION['']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td></td>
				<td><?php echo ("{$_SESSION['']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td></td>
				<td><?php echo ("{$_SESSION['']}" . "<br />"); ?></td>
			</tr>
			<tr>
				<td></td>
				<td><?php echo ("{$_SESSION['']}" . "<br />"); ?></td>
			</tr> -->
		</tbody>
	</table>
	<br>
	<div class="col-md-4 text-right">

		<button type="button" class="btn btn-success" id= "confirmBookingButton" name= "confirmBookingButton">Confirm Booking !</button>

		<!-- <button type="button" class="btn btn-success" onclick="location.href='<?php #echo site_url('CheckAvailabilityController/submitBooking');?>';" id= "confirmBookingButton" name= "confirmBookingButton">Confirm Booking !</button> -->

	</div>
	<br><br><br>
	</div>
	<div class="col-sm-2"></div>
</section>


<script type="text/javascript">
	$(document).on('click', '#confirmBookingButton', function(){

		$.ajax({
			method: 'POST',
			url: '<?php echo site_url('CheckAvailabilityController/submitBooking'); ?>',
			contentType: 'application/json',
		})
		.then(function(result) {
			console.log('<?php echo site_url('CheckAvailabilityController/index'); ?>');
			window.location = '<?php echo site_url('CheckAvailabilityController/index'); ?>'
		})
		.fail(function(err){
			console.log('err', err);
		})
	})
</script>