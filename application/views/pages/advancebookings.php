<div id="navbar">
		<a href="<?php echo site_url('CheckAvailabilityController'); ?>">Check Availability</a>
		<a href="<?php echo site_url('GetAllPendingTransactionsController'); ?>">Confirmed Bookings</a>
		<a href="<?php echo site_url('CompletedTransactionsController'); ?>">Bill</a>
		<a class="active" href="<?php echo site_url('CheckAvailabilityController/getAdvanceBookings'); ?>">Advance Bookings</a>
		<a href="<?php echo site_url('LoginController/logout'); ?>">Logout</a>
	</div>

<section>
	<br>
	<div class="col-sm-12">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Transaction ID</th>
					<th>Name</th>
					<th>Checkin</th>
					<th>Rooms</th>
					<th>Checkout</th>
					<th>View</th>
					<th>Cancel</th>
				</tr>
			</thead>
			<tbody>
				<?php
foreach ($result as $item) {
    print_r('<tr><td>');
    print_r($item->id);
    print_r('</td>');

    print_r('<td>');
    print_r($item->name);
    print_r('</td>');

    print_r('<td>');
    print_r(date("d/m/Y", strtotime($item->checkindate)) . ', ' . (date("h:i:s A", strtotime($item->checkintime))));
    print_r('</td>');

    print_r('<td>');
    print_r(str_replace('_', ' ', strtoupper($item->bookedrooms)));
    print_r('</td>');

    print_r('<td>');
    print_r(date("d/m/Y", strtotime($item->checkoutdate)) . ', ' . (date("h:i:s A", strtotime($item->checkouttime))));
    print_r('</td>');

    print_r('<td>');
    print_r('<button type="button" id="clearButton" name="clearButton" class="btn btn-info" value="');
    print_r($item->id);
    print_r('" data-toggle="modal" data-target="#myModal">View</button>');
    print_r('</td>');

    print_r('<td>');
    print_r('<a class="btn btn-success" target="_blank" ');
    // print_r('href="'.base_url().'GenerateInvoiceController/index?id=');
    // print_r($item->id);
    print_r('"');
    print_r('>Cancel</a>');
    print_r('</td>');
    print_r('</tr>');
}
print_r('</tbody>');
print_r('</table>');
print_r('</div>');
?>
</section>