<div id="navbar">
		<a class="active" href="<?php echo site_url('CheckAvailabilityController'); ?>">Check Availability</a>
		<a href="<?php echo site_url('GetAllPendingTransactionsController'); ?>">Confirmed Bookings</a>
		<a href="<?php echo site_url('CompletedTransactionsController'); ?>">Bill</a>
		<a href="<?php echo site_url('CheckAvailabilityController/getAdvanceBookings'); ?>">Advance Bookings</a>
		<a href="<?php echo site_url('LoginController/logout'); ?>">Logout</a>
	</div>

<section id="loginform" class="outer-wrapper">
	<div class="inner-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-4">

					<form id="formCheckRoomStatus" name="formCheckRoomStatus" method="POST">
						<div class="form-group">
							<label for="checkin">Checkin Date:</label>
							<input type="date" class="form-control" id="checkin" name="checkin">
						</div>
						<div class="form-group">
							<label for="checkout">Checkout Date:</label>
							<input type="date" class="form-control" id="checkout" name="checkout" placeholder="Password">
						</div>
					</form>

					<button id="btnGetRoomAvailability" name="btnGetRoomAvailability" class="btn btn-success">Submit</button>
					<button type="reset" class="btn btn-default"> Reset</button>

				</div>
			</div>

			<div class="row" id="divRooms" name="divRooms"></div>
			<div class= "col-sm-4 col-sm-offset-4" id="bookNowDiv">
				<form method="POST" action="<?php echo base_url() ?>CheckAvailabilityController/updateBookingInformation">
					<input type="hidden" name="check-in" id="checkindate">
					<input type="hidden" name="check-out" id="checkoutdate">
					<input type="hidden" name="rooms" id="rooms">
					<div id="showBookNowButton" name="showBookNowButton">
					</div>

				</form>
				<!-- <button type="submit" id="booknow" class="btn btn-success">Book Now !</button> -->
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">


	$("#btnGetRoomAvailability").one('click', function ()
	{
		$('#showBookNowButton').append('<button type="submit" id="booknow" name="booknow" class="btn btn-success">Book Now !</button>');
	});

	var selectedRoom=[];

	// function appendData() {
	// 	return true;
	// }

	$(document).on( 'click' ,  '#roomDiv' , function()
	{
			//Check if room is already selected
			bgColor = $(this).css('background-color');
			console.log(bgColor);
			if(bgColor=='rgb(128, 128, 128)')
			{
				selectedRoom.push($(this).data('for'));
				$(this).css("background", "green");
				$("#rooms").val(selectedRoom);
			}
			else
			{
				selectedRoom.splice($.inArray($(this).data('for'), selectedRoom),1);
				$(this).css("background", "grey");
				$("#rooms").val(selectedRoom);
			}
			console.log(selectedRoom);
		});



	$(document).on('click', '#btnGetRoomAvailability' , function(){
		var formData=$('#formCheckRoomStatus').serializeJSON();
		console.log(formData);
		console.log(formData['checkin']);
		console.log(formData['checkout']);

		// var isShowBookNowButton= true;

		// if (isShowBookNowButton) {
		// 	$('#showBookNowButton').append('<button type="submit" id="booknow" name="booknow" class="btn btn-success">Book Now !</button>');
		// 	isShowBookNowButton= false;
		// }


		$.ajax({
			url : '<?php echo site_url('CheckAvailabilityController/getBookingStatus'); ?>',
			type: 'POST',
			data: {
				data: formData,
			},
			success: function(response)
			{
				$("#checkindate").val(formData['checkin']);
				$("#checkoutdate").val(formData['checkout']);
				console.log('formData');
				console.log(formData);
				console.log(response);
				var data=JSON.parse(response);
				console.log(data);
				console.log(Object.keys(data).length);
				$('#divRooms').html('');

				$.each(data, function(key, value)
				{
					console.log(key, value);
				});

				$('#divRooms').append('<br/><div class="clearfix"></div>');
				// $('#bookNowDiv').append('<br><form action="<?php echo base_url() ?>CheckAvailabilityController/updateBookingInformation"><button type="submit" id="booknow" class="btn btn-success">Book Now!</button></form>');
				$.each(data, function(key, value)
				{
					if(value== false)
					{
						$('#divRooms').append('<div class="col-md-3 room-number-booked" data-for="'+key+'"><p>'+key.toUpperCase().replace("_", " ")+'</p></div>');
					}else{
						$('#divRooms').append('<div class="col-md-3 room-number" data-for="'+key+'" id="roomDiv"><p>'+key.toUpperCase().replace("_", " ")+'</p></div>');
					}
				})
			},
			fail: function()
			{
				swal( 'Error', 'An error occured' , 'error');
			}

		});
	});


</script>