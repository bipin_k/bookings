<!DOCTYPE html>
<html>
<head>
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/2.9.0/jquery.serializejson.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

 <style type="text/css" media="print" orientation="landscape">
 @media screen and (orientation: landscape) {
   html, body {
    width : 100%;
    height: 100%;
  }
  /* @page {size:landscape}  */
  h1 {
    font-size: 26px;
  }
  
}

@media print
{    
  .no-print, .no-print *
  {
    display: none !important;
  }
}

@media print {
  a[href]:after {
    content: none !important;
  }
}

@media print {
    #Header, #Footer { display: none !important; }
}

</style>


</head>
<body>
  <div class="container">
    <div class="text-center">
    <h1>Hotel Shivaay Residency</h1>
  </div>
    <div class="row">
      <div class="col-sm-4 col-xs-4">
        <!-- <h3>Hotel Shivaay Residency,</h3> -->
        NH28, Near Jalkal Road, Khalilabad, 
      Sant Kabir Nagar, Uttar Pradesh 272175
      <p>hotelshivaayresidencykld@gmail.com</p>
    </div>
    <div class="col-sm-4 col-xs-4 col-sm-offset-4 col-xs-offset-4">
      <strong>Bill No. </strong><?php print_r($user->billid); ?><br>
      <strong>GST No. </strong>09BOUPC3734G1ZQ<br>
      <strong>Transaction Id </strong><?php print_r($user->id); ?><br>
      <!-- <strong>Generated on:</strong> -->
      <!-- <br> -->
      <strong>+917081522853, +919151260784</strong>
    </div>
  </div>
  <!-- <hr/> -->
  <!-- <div class="card"> -->
<!--       <div class="card-header">

</div> -->

<div class="card-body">



  <div class="table-responsive-sm">
    <table class="table table-striped">
      <thead>
        <tr>
          <th class="center">#</th>
          <th>Item</th>
          <th>Description</th>

          <th class="right">Cost</th>
          <th class="center">Qty</th>
          <th class="right">Total</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="center">1</td>
          <td class="left strong">Customer</td>
          <td class="left"><?php print_r($user->name); ?></td>
          <td class="right"></td>
          <td class="center"></td>
          <td class="right"></td>
        </tr>
        <tr>
          <td class="center">2</td>
          <td class="left">Rooms</td>
          <td class="left"><?php print_r(str_replace(",", ", ", str_replace("_", " ", strtoupper($user->bookedrooms)))); ?></td>
          <td class="right"></td>
          <td class="center"></td>
          <td class="right"></td>
        </tr>
        <tr>
          <td class="center">3</td>
          <td class="left">Stay Duration</td>
          <td class="left"><?php print_r(substr($user->duration, 33)); ?></td>
          <td class="right"></td>
          <td class="center"></td>
          <td class="right"></td>
        </tr>
        <tr>
          <td class="center">4</td>
          <td class="left">Checkin</td>
          <td class="left"><?php print_r(date("d/m/Y", strtotime($user->checkindate)).', '.(date("h:i:s A", strtotime($user->checkintime)))); ?></td>
          <td class="right"></td>
          <td class="center"></td>
          <td class="right"></td>
        </tr>
        <tr>
          <td class="center">5</td>
          <td class="left">Checkout</td>
          <td class="left"><?php print_r(date("d/m/Y", strtotime($user->checkoutdate)).', '.(date("h:i:s A", strtotime($user->checkouttime)))); ?></td>
          <td class="right"></td>
          <td class="center"></td>
          <td class="right"></td>
        </tr>



        <?php
        $bookedroomsList= explode(',', $user->bookedrooms);
        $index= 6;

        for ($x = 0; $x < sizeof($bookedroomsList); $x++) 
        {
          echo '<tr><td class="center">';
          $index= $index+1;
          print_r($index);
          echo '</td>
          <td class="left">Billing on</td>
          <td class="left">';
          print_r(strtoupper(str_replace("_", " ", $bookedroomsList[$x])));
          echo '</td>
          <td class="right">Rs.';
          for($y= 0; $y< 20; $y++)
          {
            if ($roomprice[$y]->id == $bookedroomsList[$x]) {
              print_r($roomprice[$y]->price);
              echo '/-</td>
              <td class="center">';
              print_r($user->dayscount);
              echo '</td>
              <td class="right">Rs.';
              print_r(($roomprice[$y]->price) * ($user->dayscount));
              echo '/-</td></tr>';
            }
          }
                // print_r();

        }
        echo '<tr>
        <td class="center">';
        print_r($index+1);
        echo '</td>
        <td class="left">Tax Charged</td>
        <td class="left">CGST @ 6.00%</td>
        <td class="right"></td>
        <td class="center"></td>
        <td class="right">Rs. ';
        print_r(($user->tax)/2);
        echo '/-</td></tr>';

        echo '<tr>
        <td class="center">';
        print_r($index+2);
        echo '</td>
        <td class="left">Tax Charged</td>
        <td class="left">SGST @ 6.00%</td>
        <td class="right"></td>
        <td class="center"></td>
        <td class="right">Rs. ';
        print_r(($user->tax)/2);
        echo '/-</td></tr>';

        ?>






      </tbody>
    </table>
  </div>
  <div class="row">
    <div class="col-lg-4 col-sm-5">

    </div>

    <div class="col-lg-4 col-sm-5 ml-auto">
      <table class="table table-clear">
        <tbody>
          <tr>
            <td class="left">
              <strong>Total Amount</strong>
            </td>
            <td class="right">Rs. <?php print_r($user->advance+$user->pending); ?>/-</td>
          </tr>
          <tr>
            <td class="left">
              <strong>Advance Amount</strong>
            </td>
            <td class="right">Rs. <?php print_r($user->advance); ?>/-</td>
          </tr>
          <tr>
            <td class="left">
             <strong>Discount Amount</strong>
           </td>
           <td class="right">Rs. <?php print_r($user->discount); ?>/-</td>
         </tr>
         <tr>
          <td class="left">
            <strong>Amount Payable</strong>
          </td>
          <td class="right">
            <strong>Rs. <?php print_r($user->pending-$user->discount); ?>/-</strong>
          </td>
        </tr>
      </tbody>
    </table>

  </div>

</div>

</div>
</div>
<br><br><br>
<div class="row" style="margin-bottom: 10px;">
  <div class="col-md-6 col-xs-6"><b>Hotel Signature</b></div>
  <div class="col-md-6 col-xs-6 text-right">
    <b>Customer Signature</b><br>
    <?php 
      date_default_timezone_set('Asia/Calcutta');
      $time=round(microtime(true));
      echo date("d/m/Y, h:i:s A", $time);
      ?><br>
  </div>
</div>

<!-- <hr/> -->

<div name="termsandcondition">

  <strong>Terms and Conditions:</strong><br>
  1. Bookings are done for 24 hours period.<br>
  2. Any damage to the hotel property will be borne by the individual.

</div>
<br><br>

<button type="button" class="btn btn-success no-print" onclick="myFunction()">Print Bill!</button>
<br><br><br><br>
</div>
</body
</html>

<script type="text/javascript">
  function myFunction() {
    window.print();
  }
</script>

