
<div id="navbar">
	<a href="<?php echo site_url('CheckAvailabilityController'); ?>">Check Availability</a>
	<a href="<?php echo site_url('GetAllPendingTransactionsController'); ?>">Confirmed Bookings</a>
	<a class="active" href="<?php echo site_url('CompletedTransactionsController'); ?>">Bill</a>
	<a href="<?php echo site_url('CheckAvailabilityController/getAdvanceBookings'); ?>">Advance Bookings</a>
	<a href="<?php echo site_url('LoginController/logout'); ?>">Logout</a>
</div>

<section>
	<br>
	<div class="col-sm-12">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Transaction ID</th>
					<th>Name</th>
					<th>Checkin</th>
					<th>Rooms</th>
					<th>Checkout</th>
					<th>View</th>
					<th>Print</th>
				</tr>
			</thead>
			<tbody>
				<?php
foreach ($data as $item) {
    print_r('<tr><td>');
    print_r($item->id);
    print_r('</td>');

    print_r('<td>');
    print_r($item->name);
    print_r('</td>');

    print_r('<td>');
    print_r(date("d/m/Y", strtotime($item->checkindate)) . ', ' . (date("h:i:s A", strtotime($item->checkintime))));
    print_r('</td>');

    print_r('<td>');
    print_r(str_replace('_', ' ', strtoupper($item->bookedrooms)));
    print_r('</td>');

    print_r('<td>');
    print_r(date("d/m/Y", strtotime($item->checkoutdate)) . ', ' . (date("h:i:s A", strtotime($item->checkouttime))));
    print_r('</td>');

    print_r('<td>');
    print_r('<button type="button" id="clearButton" name="clearButton" class="btn btn-info" value="');
    print_r($item->id);
    print_r('" data-toggle="modal" data-target="#myModal">View</button>');
    print_r('</td>');

    print_r('<td>');
    print_r('<a class="btn btn-success" target="_blank" ');
    print_r('href="' . base_url() . 'GenerateInvoiceController/index?id=');
    print_r($item->id);
    print_r('"');
    print_r('>Print Bill</a>');
    print_r('</td>');
    print_r('</tr>');
}
print_r('</tbody>');
print_r('</table>');
print_r('</div>');
?>
			</section>



			<script type="text/javascript">

				$('#myModal').on('hidden.bs.modal', function () {
					location.reload();
				});


				$(document).on('click', '#clearButton', function()
				{
					var me = $(this);

					$.ajax({
						url : '<?php echo site_url('CompletedTransactionsController/getCompletedTransactionsForId'); ?>',
						type: 'GET',
						data: {
							id: me.val(),
						},
						success: function(response)
						{
							var result = parseInt(response.pending) + parseInt(response.advance);
							var pendingAmount= parseInt(response.pending) - parseInt(response.discount);
							console.log(result );
							console.log(response);
							$('<tr><td>Bill No.</td><td>'+response.id+'</td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Name</td><td>'+response.name+'</td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Email</td><td>'+response.email+'</td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Mobile</td><td>'+response.mobile+'</td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Dependents</td><td>'+response.dependents+'</td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Document Type</td><td>'+response.documentType+'</td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Other Document</td><td>'+response.otherdoc+'</td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Document Number</td><td>'+response.documentNumber+'</td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Address</td><td>'+response.address+'</td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Purpose</td><td>'+response.purpose+'</td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Checkin</td><td>'+response.checkindate+', '+response.checkintime+'</td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Checkout</td><td>'+response.checkoutdate+', '+response.checkouttime+'</td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Total Amount</td><td><b>Rs. '+ result+'/-</b></td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Booking Amount</td><td><b>Rs. '+response.bookingamount+'/- </b></td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Discount Amount</td><td><b>Rs. '+response.discount+'/- </b></td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Pending Amount</td><td><b>Rs. '+pendingAmount+'/- </b></td></tr>').appendTo($("#modalTable"));
							$('<tr><td>Booked Rooms</td><td>'+response.bookedrooms.toUpperCase().split('_').join(' ')+'</td></tr>').appendTo($("#modalTable"));
						},
						fail: function()
						{
							swal( 'Error', 'An error occured' , 'error');
						}
					});
				});
			</script>