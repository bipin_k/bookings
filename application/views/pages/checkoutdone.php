<section>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<table class="table table-striped">
			<tbody>
				<tr>
					<td>Transaction Id</td>
					<td><?php print_r($data->id); ?></td>
				</tr>
				<tr>
					<td>Name</td>
					<td><?php print_r($data->name); ?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><?php print_r($data->email); ?></td>
				</tr>
				<tr>
					<td>Mobile</td>
					<td><?php print_r($data->mobile); ?></td>
				</tr>
				<tr>
					<td>No. of Dependents</td>
					<td><?php print_r($data->dependents); ?></td>
				</tr>
				<tr>
					<td>Document Type</td>
					<td><?php print_r($data->documentType); ?></td>
				</tr>
				<tr>
					<td>Other, if applicable</td>
					<td><?php print_r($data->otherdoc); ?></td>
				</tr>
				<tr>
					<td>Document Number</td>
					<td><?php print_r($data->documentNumber); ?></td>
				</tr>
				<tr>
					<td>Address</td>
					<td><?php print_r($data->address); ?></td>
				</tr>
				<tr>
					<td>Purpose</td>
					<td><?php print_r($data->purpose); ?></td>
				</tr>
				<tr>
					<td>Booked Rooms</td>
					<td><?php print_r(str_replace(",", ", ", str_replace("_", " ", strtoupper($data->bookedrooms)))); ?></td>
				</tr>
				<tr>
					<td>Checkin Date</td>
					<td><?php print_r(date("d/m/Y", strtotime($data->checkindate)) . ', '); print_r(date("l", strtotime($data->checkindate))); ?></td>
					
				</tr>
				<tr>
					<td>Checkin Time</td>
					<td><?php date_default_timezone_set('Asia/Calcutta'); print_r(date("h:i:s A", strtotime($data->checkintime)));?></td>
				</tr>

				<tr>
					<td>Checkout Date</td>
					<td><?php print_r(date("d/m/Y", strtotime($data->checkoutdate)) . ', '); print_r(date("l", strtotime($data->checkoutdate))); ?></td>
				</tr>

				<tr>
					<td>Checkout Time</td>
					<td><?php date_default_timezone_set('Asia/Calcutta'); print_r(date("h:i:s A", strtotime($data->checkouttime)));?></td>

				</tr>
				<tr>
					<td>Stay Hours</td>
					<td><b><?php print_r($paymentData->duration); ?></b></td>
				</tr>
				<tr>
					<td>Taxable Amount</td>
					<td><b>Rs. <?php print_r($paymentData->tax); ?>/- Only</b></td>
				</tr>
				<tr>
					<td>Final Amount</td>
					<td><b>Rs. <?php print_r($paymentData->amount); ?>/- Only</b></td>
				</tr>
				<tr>
					<td>Advance Amount</td>
					<td><b>Rs. <?php print_r($data->bookingamount); ?>/- Only</b></td>
				</tr>
				<tr>
					<td>Discount Amount</td>
					<td><b>Rs. <?php print_r($data->discount); ?>/- Only</b></td>
				</tr>

				<tr>
					<td>Pending Amount</td>
					<td><b>Rs. <?php print_r($data->pending - $data->discount); ?>/- Only</b></td>
				</tr>
			</tbody>
		</table>
		<br><br>
		<button type="button" class="btn btn-success" id="submitFinally" name="submitFinally">Success</button>
		<br><br><br><br>
	</div>
	<div class="col-sm-2"></div>

</div>
</section>


<script type="text/javascript">
	$(document).on('click', '#submitFinally', function(){
		event.preventDefault();
		var formData={
			'duration': '<?php print_r($paymentData->duration); ?>',
			'id': '<?php print_r($data->id); ?>',
			'checkinDate': '<?php print_r($data->checkindate); ?>',
			'checkoutDate': '<?php print_r($data->checkoutdate); ?>',
			'initialcheckoutdate': '<?php print_r($data->initialCheckoutDate); ?>',
			'room':'<?php print_r($data->bookedrooms); ?>',
			'taxableAmount': '<?php print_r($paymentData->tax); ?>'
		};
		console.log(formData);

		$.ajax({
			method: 'POST',
			url: '<?php echo site_url('ChekoutPageController/checkoutRoomForUser');?>',
			data: {
				data: formData,
			}
		})
		.then(function(result) {
			window.location = '<?php echo site_url('CheckAvailabilityController/index'); ?>'
		})
		.fail(function(err){
			console.log('err', err);
		})
	})
</script>