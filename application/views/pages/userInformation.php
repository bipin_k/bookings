<section id="loginform" class="outer-wrapper">


	<br>
	<form class="form-horizontal" id="userInformationForm" name="userInformationForm" method="POST" action="<?php echo base_url()?>CheckAvailabilityController/getAllBookedInformation">
		<div class="form-group">
			<label class="control-label col-sm-2" for="pwd">Name:</label>
			<div class="col-sm-4">          
				<input type="text" class="form-control" id="name" name="name" placeholder="Enter name!">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for="email">Email:</label>
			<div class="col-sm-4">
				<input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
			</div>
		</div>

		<div class="form-group" id="optionforotherdoc">
			<label class="control-label col-sm-2" for="pwd">Mobile:</label>
			<div class="col-sm-4">          
				<input type="number" class="form-control" id="mobile" name="mobile" placeholder="Enter mobile!">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for="dependents">No. of Dependents:</label>
			<div class="col-sm-1"> 
				<select class="form-control" id="dependents" name="dependents">
					<option>0</option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
					<option>6</option>
					<option>7</option>
					<option>8</option>
					<option>9</option>
					<option>10</option>
				</select>
			</div>
		</div>




		<div class="form-group">
			<label class="control-label col-sm-2" for="documentType">Document Type:</label>
			<div class="col-sm-4"> 
				<select class="form-control" id="documentType" name="documentType">
					<option>Aadhaar Card</option>
					<option>Bank Account Passbook</option>
					<option>Driving License</option>
					<option>Passport</option>
					<option>Student ID Card</option>
					<option>Voter ID Card</option>
					<option>Other</option>
				</select>
			</div>
		</div>

		<div class="form-group" id="otherType"></div>
		<div class="form-group" id="documentnumbersection">
			<label class="control-label col-sm-2" for="documentNumber">Document Id:</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" id="documentNumber" name="documentNumber" placeholder="Enter Document Id!">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for="address">Address:</label>
			<div class="col-sm-4">   
				<textarea class="form-control" rows="5" id="address" name="address"></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for="pwd">Purpose of visit:</label>
			<div class="col-sm-4">          
				<input type="text" class="form-control" id="purpose" name="purpose" placeholder="Enter purpose of visit!">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for="checkinDate">Checkin Date:</label>
			<div class="col-sm-4">
				<div name="checkinDate">
					<?php echo date("d-m-Y", strtotime($checkin)); ?>
					<!-- <?php echo date("d-m-Y", strtotime($checkin)). ', ' . date("l", strtotime($checkin)); ?> -->
				</div>
			</div>
		</div>


		<div class="form-group">
			<label class="control-label col-sm-2" for="checkintime">Checkin Time:</label>
			<div class="col-sm-1"> 
				<select class="form-control" id="checkintime" name="checkintime">
					<option>00:00:00</option>
					<option>00:30:00</option>
					<option>01:00:00</option>
					<option>01:30:00</option>
					<option>02:00:00</option>
					<option>02:30:00</option>
					<option>03:00:00</option>
					<option>03:30:00</option>
					<option>04:00:00</option>
					<option>04:30:00</option>
					<option>05:00:00</option>
					<option>05:30:00</option>
					<option>06:00:00</option>
					<option>06:30:00</option>
					<option>07:00:00</option>
					<option>07:30:00</option>
					<option>08:00:00</option>
					<option>08:30:00</option>
					<option>09:00:00</option>
					<option>09:30:00</option>
					<option>10:00:00</option>
					<option>10:30:00</option>
					<option>11:00:00</option>
					<option>11:30:00</option>
					<option>12:00:00</option>
					<option>12:30:00</option>
					<option>13:00:00</option>
					<option>13:30:00</option>
					<option>14:00:00</option>
					<option>14:30:00</option>
					<option>15:00:00</option>
					<option>15:30:00</option>
					<option>16:00:00</option>
					<option>16:30:00</option>
					<option>17:00:00</option>
					<option>17:30:00</option>
					<option>18:00:00</option>
					<option>18:30:00</option>
					<option>19:00:00</option>
					<option>19:30:00</option>
					<option>20:00:00</option>
					<option>20:30:00</option>
					<option>21:00:00</option>
					<option>21:30:00</option>
					<option>22:00:00</option>
					<option>22:30:00</option>
					<option>23:00:00</option>
					<option>23:30:00</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for="checkinDate">Checkout Date:</label>
			<div class="col-sm-4">
				<data name="checkinDate">
					<?php echo date("d-m-Y", strtotime($checkout)); ?>
					<!-- <?php echo date("d-m-Y", strtotime($checkout)). ', ' . date("l", strtotime($checkout)); ?> -->
				</data>
			</div>
		</div>



		<div class="form-group">
			<label class="control-label col-sm-2" for="checkouttime">Checkout Time:</label>
			<div class="col-sm-1"> 
				<select class="form-control" id="checkouttime" name="checkouttime">
					<option>00:00:00</option>
					<option>00:30:00</option>
					<option>01:00:00</option>
					<option>01:30:00</option>
					<option>02:00:00</option>
					<option>02:30:00</option>
					<option>03:00:00</option>
					<option>03:30:00</option>
					<option>04:00:00</option>
					<option>04:30:00</option>
					<option>05:00:00</option>
					<option>05:30:00</option>
					<option>06:00:00</option>
					<option>06:30:00</option>
					<option>07:00:00</option>
					<option>07:30:00</option>
					<option>08:00:00</option>
					<option>08:30:00</option>
					<option>09:00:00</option>
					<option>09:30:00</option>
					<option>10:00:00</option>
					<option>10:30:00</option>
					<option>11:00:00</option>
					<option>11:30:00</option>
					<option>12:00:00</option>
					<option>12:30:00</option>
					<option>13:00:00</option>
					<option>13:30:00</option>
					<option>14:00:00</option>
					<option>14:30:00</option>
					<option>15:00:00</option>
					<option>15:30:00</option>
					<option>16:00:00</option>
					<option>16:30:00</option>
					<option>17:00:00</option>
					<option>17:30:00</option>
					<option>18:00:00</option>
					<option>18:30:00</option>
					<option>19:00:00</option>
					<option>19:30:00</option>
					<option>20:00:00</option>
					<option>20:30:00</option>
					<option>21:00:00</option>
					<option>21:30:00</option>
					<option>22:00:00</option>
					<option>22:30:00</option>
					<option>23:00:00</option>
					<option>23:30:00</option>
				</select>
			</div>
		</div>

		<div class="form-group" id="optionforotherdoc">
			<label class="control-label col-sm-2" for="bookingamount">Booking Amount:</label>
			<div class="col-sm-2">          
				<input type="number" class="form-control" id="bookingamount" name="bookingamount" placeholder="Enter Booking Amount!">
			</div>
		</div>


		<div class="form-group">        
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" id="submitUserInformationForm" name="submitUserInformationForm" class="btn btn-success">Save</button>
				<!-- <button type="reset" class="btn btn-default">Reset</button> -->
				<button onclick="location.href='<?php echo site_url('CheckAvailabilityController/submissionPage');?>';" id= "nextButton" name= "nextButton" class="btn btn-info" disabled="true">Next</button>
			</div>
		</div>
	</form>

	<!-- <h2>JSON</h2>
	<pre id="result">
	</pre> -->

</section>

<script type="text/javascript">

	$(document).on('click', '#submitUserInformationForm', function(){
		$("#nextButton").prop('disabled',false);

		var formData= $('#userInformationForm').serializeJSON();
		console.log(formData);

		$.ajax({
			type: 'POST', 
			url: '<?php echo site_url('CheckAvailabilityController/getAllBookedInformation'); ?>',
			data: {
				data: formData,
			},
			success: function(result)
			{
				window.location = '<?php echo site_url('CheckAvailabilityController/getAllBookedInformation'); ?>'
			}
		})
		.done(function(data){
			console.log(data);
		});
	})


	// $('.form-horizontal').submit(function(e) 
	// {
	// 	e.preventDefault();
	// 	// $(this).serialize(); will be the serialized form
	// 	$(this).append($(this).serialize() + '<br />');
	// });

	$.fn.serializeObject = function()
	{
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		// console.log(o);
		return o;
	};

	$(function() {
		$('form').submit(function() {
			$('#result').text(JSON.stringify($('form').serializeObject()));
			return false;
		});
	});







	// var counter= false;
	// $('select').on('change', function (e){
	// 	if (counter==false) {
	// 		$('#documentnumbersection').append('<label class="control-label col-sm-2" for="pwd">Document Id:</label><div class="col-sm-4"><input type="text" class="form-control" id="documentNumber" name="documentNumber" placeholder="Enter Document Id!"></div>');
	// 		counter=true;
	// 	}
	// });

	$('#documentType').on('change',function(){
        //var optionValue = $(this).val();
        //var optionText = $('#dropdownList option[value="'+optionValue+'"]').text();
        // var optionText = $("#documentType option:selected").text();
        // alert("Selected Option Text: "+optionText);

        if ($("#documentType option:selected").text()==='Other') 
        {
        	console.log(true);
        	$('#otherType').append('<label class="control-label col-sm-2" for="otherdoc">If selected, other:</label><div class="col-sm-4"><input type="text" class="form-control" id="otherdoc" name="otherdoc" placeholder="If selected, other!"></div>');
        }
        else
        {
        	console.log(false);
        	// $('#otherType').remove();
        	// $(this).parents('#otherType').remove();

        	// $('#otherType').hide('<label class="control-label col-sm-2" for="otherdoc">If selected, other:</label><div class="col-sm-4"><input type="number" class="form-control" id="otherdoc" name="otherdoc" placeholder="If selected, other!"></div>');
        }
    }); 
</script>