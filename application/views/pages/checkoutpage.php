<section>

	<br>Please confirm following information!<br><br>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<table class="table table-striped">
			<!-- <thead>
				<tr>
					<th>Key</th>
					<th>Value</th>
				</tr>
			</thead> -->
			<tbody>
				<tr>
					<td>Transaction Id</td>
					<td><?php print_r($transaction->id); ?></td>
				</tr>
				<tr>
					<td>Name</td>
					<td><?php print_r($transaction->name); ?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><?php print_r($transaction->email); ?></td>
				</tr>
				<tr>
					<td>Mobile</td>
					<td><?php print_r($transaction->mobile); ?></td>
				</tr>
				<tr>
					<td>No. of Dependents</td>
					<td><?php print_r($transaction->dependents); ?></td>
				</tr>
				<tr>
					<td>Document Type</td>
					<td><?php print_r($transaction->documentType); ?></td>
				</tr>
				<tr>
					<td>Other, if applicable</td>
					<td><?php print_r($transaction->otherdoc); ?></td>
				</tr>
				<tr>
					<td>Document Number</td>
					<td><?php print_r($transaction->documentNumber); ?></td>
				</tr>
				<tr>
					<td>Address</td>
					<td><?php print_r($transaction->address); ?></td>
				</tr>
				<tr>
					<td>Purpose</td>
					<td><?php print_r($transaction->purpose); ?></td>
				</tr>
				<tr>
					<td>Booked Rooms</td>
					<td><?php print_r(str_replace(",", ", ", str_replace("_", " ", strtoupper($transaction->bookedrooms)))); ?></td>
				</tr>
				<tr>
					<td>Checkin Date</td>
					<td><?php print_r(date("d/m/Y", strtotime($transaction->checkindate)) . ', '); print_r(date("l", strtotime($transaction->checkindate))); ?></td>
					
				</tr>
				<tr>
					<td>Checkin Time</td>
					<td><?php print_r($transaction->checkintime . ' HOURS'); ?></td>
				</tr>

				<tr>
					<td>Checkout Date</td>
					<td><?php $checkout= explode(" ", $final_amount->checkout); print_r(date("d/m/Y", strtotime($checkout[0])).', '); print_r(date("l", strtotime($checkout[0]))); ?></td>
				</tr>

				<tr>
					<td>Checkout Time</td>
					<td><?php print_r($checkout[1] . ' HOURS'); ?></td>
				</tr>
				<tr>
					<td>Stay Hours</td>
					<td><b><?php print_r($final_amount->duration); ?></b></td>
				</tr>
				<tr>
					<td>Total days of Stay</td>
					<td><b><?php print_r($final_amount->daysCount); ?></b></td>
				</tr>
				<tr>
					<td>Final Amount</td>
					<td><b>Rs. <?php print_r($final_amount->amount); ?>/- Only</b></td>
				</tr>
				<tr>
					<td>Advance Amount</td>
					<td><b>Rs. <?php print_r($transaction->bookingamount); ?>/- Only</b></td>
				</tr>
				<tr>
					<td>Pending Amount</td>
					<td><b>Rs. <?php print_r($final_amount->amount - $transaction->bookingamount); ?>/- Only</b></td>
				</tr>
			</tbody>
		</table>
		<form method="post" class="form-inline" action="<?php echo base_url()?>ChekoutPageController/finalsubmit?id=<?php print_r($transaction->id); ?>">
			<div class="form-group">
				<label for="usr">Enter amount for discount, if applicable:</label>
				<input type="number" name="discount" id="discount" class="form-control" id="usr">
			</div>
			<br><br><br>
			

			<button type="submit" class="btn btn-success" location.href="<?php echo base_url()?>ChekoutPageController/finalsubmit?id=<?php print_r($transaction->id); ?>">Next</button>
		</form>
		<br><br><br>
	</div>
	<div class="col-sm-2"></div>
</div>
	<!-- <?php 
	#print_r($transaction->id);
	
	#print_r($final_amount->duration);
	// print_r("</pre>");
	?> -->
	

	<script type="text/javascript">
		// magic.js
		$(document).ready(function() 
		{

			$('form').submit(function(event) {
				// event.preventDefault();


				var formData = {
					'discount' : $('input[name=discount]').val(),
					'id' : <?php print_r($transaction->id); ?>,
					'pending' : <?php print_r($final_amount->amount - $transaction->bookingamount); ?>,
					'checkoutdate': '<?php print_r($checkout[0]); ?>',
					'checkouttime' : '<?php print_r($checkout[1]); ?>',
					'daysNumber' : <?php print_r($final_amount->daysCount); ?>
				}

				console.log(formData);


				$.ajax({
					type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            		url         : 'submitdiscount', // the url where we want to POST
           			data        : formData, // our data object
            		dataType    : 'json', // what type of data do we expect back from the server
            		encode      : true
            	})
            	.done(function(data)
            	{
            		console.log(data);
            	})
				// .then(function(result) {
				// 	// console.log('<?php #echo site_url('CheckAvailabilityController/index'); ?>');
				// 	window.location = '<?php #echo site_url('ChekoutPageController/finalsubmit?id=' . $transaction->id); ?>'
				// })
				// .fail(function(err){
				// 	console.log('err', err);
				// })
            	
            	
            });
		});
	</script>

</section>