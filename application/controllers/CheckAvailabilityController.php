<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CheckAvailabilityController extends CI_Controller
{
    public $data;
    public $txt = "Hello world!";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('HotelBookingStatusModel');
        $this->load->library('session');
    }

    public function index()
    {

        if (!$this->session->userdata('logged_in')) {
            redirect('LoginController/logout');
        }

        $this->load->helper('form');
        $this->load->view('templates/header');
        $this->load->view('pages/checkavailability');
        $this->load->view('templates/footer');
    }

    public function updateBookingInformation()
    {
        $data = $this->input->post();
        $this->session->set_userdata('checkin', $data['check-in']);
        $this->session->set_userdata('checkout', $data['check-out']);
        $this->session->set_userdata('rooms', $data['rooms']);
        $firstPage = $this->session->userdata();
        // var_dump($firstPage);
        // var_dump($this->session->userdata('checkin'));
        // var_dump($firstPage['checkin']);

        // $this->session->set_userdata(json_encode($jsondecoded));

        $this->load->helper('form');
        $this->load->view('templates/header');
        $this->load->view('pages/userInformation', $firstPage);
        $this->load->view('templates/footer');
    }

    public function getBookingStatus()
    {
        //FetchData from post
        if ($_POST != '') {
            $data = $this->input->post('data');

            $this->load->model('HotelBookingStatusModel');
            $datesArray = array();
            $date_from = $data['checkin'];
            $date_to = $data['checkout'];
            $date_from = strtotime($date_from); // Convert date to a UNIX timestamp
            $date_to = strtotime($date_to); // Convert date to a UNIX timestamp

            for ($i = $date_from; $i <= $date_to; $i += 86400) {
                array_push($datesArray, date("Y-m-d", $i));
            }

            for ($i = 0; $i < count($datesArray); $i++) {
                $jsonObject = json_encode($this->HotelBookingStatusModel->checkIfDateIsAvailable($datesArray[$i]));
                $decodedJSON = json_decode($jsonObject, true);
                if (!$decodedJSON['status']) {
                    $this->HotelBookingStatusModel->insertDate($datesArray[$i]);
                }
            }

            $result = json_encode($this->HotelBookingStatusModel->getBookingStatus($data['checkin'], $data['checkout']));
            $jsondecoded = json_decode($result, true);
            $finaljson = array();

            foreach ($jsondecoded as $masterkey => $mastervalue) {
                foreach ($mastervalue as $key => $value) {
                    if ($key != 'date') {
                        $finaljson[$key] = true;
                    }
                }
            }

            foreach ($jsondecoded as $masterkey => $mastervalue) {
                // echo "$masterkey";
                // echo "$mastervalue";
                // print_r($mastervalue);
                foreach ($mastervalue as $key => $value) {
                    // echo $key." = ".$value . ";";
                    // print_r($key." = ".$value . ";");
                    if ($key != 'date') {
                        // print_r($key." = ".$value . ";");
                        // print_r("\n");
                        if ($value == 1) {
                            $finaljson[$key] = false;
                        }
                    }
                }
                // print_r("\n");
            }
            print_r(json_encode($finaljson));
        } else {
            header('HTTP/1.1 500 Internal Server Error');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => 'Unable to delete school', 'code' => 1337)));
            return false;
        }
    }

    public function getAllBookedInformation()
    {
        $data = $this->input->post('data');
        $finaljson = array();
        $finaljson['checkindate'] = $this->session->userdata('checkin');
        $finaljson['checkoutdate'] = $this->session->userdata('checkout');
        $finaljson['bookedrooms'] = $this->session->userdata('rooms');

        $this->session->set_userdata('name', $data['name']);
        $finaljson['name'] = $this->session->userdata('name');

        $this->session->set_userdata('email', $data['email']);
        $finaljson['email'] = $this->session->userdata('email');

        $this->session->set_userdata('mobile', $data['mobile']);
        $finaljson['mobile'] = $this->session->userdata('mobile');

        $this->session->set_userdata('dependents', $data['dependents']);
        $finaljson['dependents'] = $this->session->userdata('dependents');

        $this->session->set_userdata('documentType', $data['documentType']);
        $finaljson['documentType'] = $this->session->userdata('documentType');

        $finaljson['otherdoc'] = ' ';

        if (array_key_exists('otherdoc', (array) $data)) {
            $this->session->set_userdata('otherdoc', $data['otherdoc']);
            $finaljson['otherdoc'] = $this->session->userdata('otherdoc');
        } else {
            $this->session->set_userdata('otherdoc', 'NA');
            $finaljson['otherdoc'] = ' ';
        }

        $this->session->set_userdata('documentNumber', $data['documentNumber']);
        $finaljson['documentNumber'] = $this->session->userdata('documentNumber');

        $text = $data['address'];
        $text = preg_replace("/[\r\n]+/", ", ", $text);
        $this->session->set_userdata('address', $text);
        $finaljson['address'] = $this->session->userdata('address');

        // $this->session->set_userdata('address',$data['address']);
        // $text = preg_replace("/[\r\n]+/", ", ", $this->session->userdata('address'));
        // $finaljson['address']= $text;

        $this->session->set_userdata('purpose', $data['purpose']);
        $finaljson['purpose'] = $this->session->userdata('purpose');

        $this->session->set_userdata('checkintime', $data['checkintime']);
        $finaljson['checkintime'] = $this->session->userdata('checkintime');

        $this->session->set_userdata('checkouttime', $data['checkouttime']);
        $finaljson['checkouttime'] = $this->session->userdata('checkouttime');

        if ($data['bookingamount'] != null) {
            $this->session->set_userdata('bookingamount', $data['bookingamount']);
            $finaljson['bookingamount'] = $this->session->userdata('bookingamount');
            # code...
        } else {
            $this->session->set_userdata('bookingamount', 0);
            $finaljson['bookingamount'] = $this->session->userdata('bookingamount');
        }

        // $this->session->set_userdata('bookingamount', $data['bookingamount']);
        // $finaljson['bookingamount']= $this->session->userdata('bookingamount');

        // $this->session->set_userdata('',$data['']);
        // $finaljson['']= $this->session->userdata('');

        // $mergedJSON= array_merge($finaljson, (array)$data);

        $this->load->view('templates/header');
        $this->load->view('pages/confirmbooking');
        $this->load->view('templates/footer');

        header('Content-Type: application/json; charset=UTF-8');
        // print_r(json_encode($mergedJSON));
        // print_r(json_encode($data, true));
        print_r(json_encode($finaljson));
    }

    public function submissionPage()
    {
        $this->load->view('templates/header');
        $this->load->view('pages/confirmbooking');
        $this->load->view('templates/footer');
    }

    // public function convertIntoTimeStamp($bookingdate, $bookingtime)
    public function convertIntoTimeStamp()
    {
        $bookingdate = $this->input->get('bookingdate');
        // $bookingdate= '2018-09-10';
        $bookingtime = $this->input->get('bookingtime');
        // $bookingtime= '04:30:00';
        $bookingSchedule = $bookingdate . ' ' . $bookingtime;

        echo $bookingSchedule;
        list($year, $month, $day) = explode('-', $bookingdate);
        list($hour, $minute, $second) = explode(':', $bookingtime);
        $timestamp = mktime($hour, $minute, $second, $month, $day, $year);
        // $dateTime = datetime::createfromformat('D, d M Y H:i:s e',$timestamp);
        print_r("\n");
        print_r("M-d-Y", mktime(0, 0, 0, 14, 1, 2001));
        print_r("\n");
        print_r($timestamp);
        print_r("\n");
        $d1 = new Datetime();
        print_r($d1);

        print_r("\n");

        echo $d1->format('U');
        # 1537015580
        print_r("\n");
        $d2 = new Datetime("now");
        echo $d2->format('U');

        // $start = strtotime('2017-08-10 10:05:25');
        $start = strtotime($bookingSchedule);
        $end = strtotime('2018-09-20 13:00:00');
        $diff = $end - $start;

        $hours = floor($diff / (60 * 60));
        $minutes = $diff - $hours * (60 * 60);
        print_r("\n");
        echo 'Remaining time: ' . $hours . ' hours, ' . floor($minutes / 60) . ' minutes';

        // print_r(strtotime($bookingSchedule));

        // print_r($bookingSchedule);
        // print_r("\n");

        // $newTime= "{$bookingtime} {$month}/{$day}/{$year}";
        // print_r(strtotime($newTime));
        // echo "{strtotime($newTime)}\n";
        // echo "{$timestamp}\n";
        // echo $bookingdate;
        // echo 'bipin';
    }

    public function submitBooking()
    {
        // echo '<pre>';
        // print_r($_SESSION);
        // print_r($_SESSION['checkin']);
        // echo '</pre>';
        $this->load->model('SubmitBookingModel');
	$billId= $this->HotelBookingStatusModel->getBillNumberModel();
        // header('Content-type: application/json');
        echo json_encode($this->SubmitBookingModel->submitBookingTransaction($billId, $_SESSION['checkin'], $_SESSION['checkout'], $_SESSION['rooms'], $_SESSION['name'], $_SESSION['email'], $_SESSION['mobile'], $_SESSION['dependents'], $_SESSION['documentType'], $_SESSION['otherdoc'], $_SESSION['documentNumber'], $_SESSION['address'], $_SESSION['purpose'], $_SESSION['checkintime'], $_SESSION['checkouttime'], $_SESSION['bookingamount']));
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('checkin');
        $this->session->unset_userdata('checkout');
        $this->session->unset_userdata('rooms');
        $this->session->unset_userdata('bookingamount');
        $this->session->unset_userdata('mobile');
        $this->session->unset_userdata('documentNumber');
        $this->session->unset_userdata('address');
        $this->session->unset_userdata('purpose');
        $this->session->unset_userdata('dependents');
        $this->session->unset_userdata('documentType');
        $this->session->unset_userdata('otherdoc');
        $this->session->unset_userdata('checkintime');
        $this->session->unset_userdata('checkouttime');
        // $this->session->unset_userdata('');
    }

    public function getAllPendingTransactions()
    {
        $this->load->model('GetAllPendingTransactionsModel');
        header('Content-type: application/json');
        echo json_encode($this->GetAllPendingTransactionsModel->getAllPendingTransactionData());
    }

    public function roomsize()
    {
        $str = $_SESSION['rooms'];
        $arr = explode(",", $str);
        $count = count($arr);
        $query = 'update bookingstatus set ';
        for ($i = 0; $i < $count; $i++) {
            $query = $query . $arr[$i] . '= true, ';
        }
        $query = rtrim($query, ", ");
        $query = $query . ' where date between ';
        print_r($query);
    }

    public function getDateBetweenTwoDates()
    {
        $this->load->model('HotelBookingStatusModel');
        $datesArray = array();
        $date_from = $this->input->get('startdate');
        $date_to = $this->input->get('enddate');
        $date_from = strtotime($date_from); // Convert date to a UNIX timestamp
        $date_to = strtotime($date_to); // Convert date to a UNIX timestamp

        for ($i = $date_from; $i <= $date_to; $i += 86400) {
            array_push($datesArray, date("Y-m-d", $i));
        }

        for ($i = 0; $i < count($datesArray); $i++) {
            $jsonObject = json_encode($this->HotelBookingStatusModel->checkIfDateIsAvailable($datesArray[$i]));
            $decodedJSON = json_decode($jsonObject, true);
            if (!$decodedJSON['status']) {
                $this->HotelBookingStatusModel->insertDate($datesArray[$i]);
            }
        }
    }

    public function getAdvanceBookings()
    {

        if (!$this->session->userdata('logged_in')) {
            redirect('LoginController/logout');
        }
        // header('Content-type: application/json');
        $data['result'] = $this->HotelBookingStatusModel->getAdvanceBookingsModel();
        // print_r($data);
        $this->load->view('templates/header');
        $this->load->view('pages/advancebookings', $data);
        $this->load->view('templates/footer');
    }
}
