<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class HotelBookingStatusController extends CI_Controller
{
    public function getBookingStatus()
    {
        $checkin = $this->input->get('checkin');
        $checkout = $this->input->get('checkout');
        $this->load->model('HotelBookingStatusModel');

        header('Content-type: application/json');
        $this->output->set_status_header(200);
        // echo json_encode($this->HotelBookingStatusModel->getBookingStatus($checkin, $checkout));
        print_r($this->HotelBookingStatusModel->getBookingStatus($checkin, $checkout));
    }
}
