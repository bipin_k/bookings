<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ChekoutPageController extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->load->model('GetAllPendingTransactionsModel');
        $this->load->library('session');
    }

    public function getCheckoutData()
    {
        $final_result = array();
    	$transactionId= $this->input->get('id');
        $final_result["transaction"] = $this->GetAllPendingTransactionsModel->getDataForTransactionId($transactionId);
        // array_push($final_result, $this->GetAllPendingTransactionsModel->getDataForTransactionId($transactionId));


        $rooms= json_encode($this->GetAllPendingTransactionsModel->getRoomPrice($transactionId));
        $jsonArray = json_decode($rooms,true);
        $firstName = $jsonArray['bookedrooms'];

        $checkinTime= $jsonArray['checkindate'].' '.$jsonArray['checkintime'];
        $start = strtotime($checkinTime);

        print_r("\n");
        date_default_timezone_set("Asia/Calcutta");  
        $checkoutTime= date('Y-m-d H:i:s');
        $end= strtotime($checkoutTime);        
        $date = date_create_from_format('Y-m-d H:i:s', $checkinTime);
        date_default_timezone_set("Asia/Calcutta"); 
        $start= $date->getTimestamp()*1000;
        $end= $end*1000;
        $diff  = $end - $start;

        $days = floor($diff / (60 * 60 * 1000*24));
        $hours = floor(($diff / (60 * 60 * 1000)) - ($days*24));
        $minutes = floor(($diff / (1000*60)) - ($days*24*60) - ($hours*60));
        // echo 'Total duration of accomodation: ' .$days . ' days, ' . $hours .  ' hours, ' . $minutes  . ' minutes';

        $myArray = explode(',', $firstName);
        $totalPrice= 0;
        for($x= 0; $x< sizeof($myArray); $x++)
        {
            $roomcost= json_encode($this->GetAllPendingTransactionsModel->getPriceOfEachRoom($myArray[$x]));
            $roomcostArray= json_decode($roomcost, true);

            if ($roomcostArray['price'] > 999) {
                $totalPrice= $totalPrice + (($roomcostArray['price'] * ($days + 1)) * 1.12);
            }
            else{
                $totalPrice= $totalPrice + ($roomcostArray['price'] * ($days + 1));
            }
        }

        $result= array("duration"=> "Total duration of accomodation is " .$days . " Days, " . $hours .  " Hours, " . $minutes  . " Minutes!", "amount"=> $totalPrice, "bookingdata"=>$jsonArray, "checkout"=>$checkoutTime, "checkoutInMillis"=>$end);

        $final_result["final_amount"] = (object)$result;

        // array_push($final_result, $result);

        // print_r($final_result);

        // header('Content-type: application/json');
        // echo json_encode($final_result);

        $this->session->set_userdata('finalAmount',$totalPrice);
        




        $this->load->view('templates/header');
        $this->load->view('pages/checkoutpage', $final_result);
        $this->load->view('templates/footer');
    }

    public function submitdiscount()
    {
        $transactionId= $this->input->post('id');
        $discountamount= $this->input->post('discount');
        $pendingamount= $this->input->post('pending');

        $checkoutDate= $this->input->post('checkoutdate');
        $checkoutTime= $this->input->post('checkouttime');

        $this->GetAllPendingTransactionsModel->updateCheckoutData($transactionId, $checkoutDate, $checkoutTime);

        if($discountamount==null)
        {
            $discountamount= 0;
        }
        echo $this->GetAllPendingTransactionsModel->updatediscount($transactionId, $discountamount, $pendingamount);
    }

    public function finalsubmit()
    {
        $transactionId= $this->input->get('id');
        // header('Content-type: application/json');
        $confirmedPayemt["data"]= $this->GetAllPendingTransactionsModel->finalSubmitForCheckout($transactionId);




        $rooms= json_encode($this->GetAllPendingTransactionsModel->getRoomPriceWithCheckoutData($transactionId));
        $jsonArray = json_decode($rooms,true);
        $firstName = $jsonArray['bookedrooms'];

        $checkinTime= $jsonArray['checkindate'].' '.$jsonArray['checkintime'];
        $start = strtotime($checkinTime);

        $checkoutTime= $jsonArray['checkoutdate'].' '.$jsonArray['checkouttime'];
        $end= strtotime($checkoutTime); 

        // print_r("\n");
        // date_default_timezone_set("Asia/Calcutta");  
        // $checkoutTime= date('Y-m-d H:i:s');
        // $end= strtotime($checkoutTime);        
        $date = date_create_from_format('Y-m-d H:i:s', $checkinTime);
        date_default_timezone_set("Asia/Calcutta"); 
        $start= $date->getTimestamp()*1000;
        $end= $end*1000;
        $diff  = $end - $start;

        $days = floor($diff / (60 * 60 * 1000*24));
        $hours = floor(($diff / (60 * 60 * 1000)) - ($days*24));
        $minutes = floor(($diff / (1000*60)) - ($days*24*60) - ($hours*60));
        // echo 'Total duration of accomodation: ' .$days . ' days, ' . $hours .  ' hours, ' . $minutes  . ' minutes';

        $myArray = explode(',', $firstName);
        $totalPrice= 0;
        $taxAmount= 0;
        for($x= 0; $x< sizeof($myArray); $x++)
        {
            $roomcost= json_encode($this->GetAllPendingTransactionsModel->getPriceOfEachRoom($myArray[$x]));
            $roomcostArray= json_decode($roomcost, true);

            if ($roomcostArray['price'] > 999) {
                $totalPrice= $totalPrice + (($roomcostArray['price'] * ($days + 1)) * 1.12);
                $taxAmount= $taxAmount + (($roomcostArray['price'] * ($days + 1)) * 0.12);
            }
            else{
                $totalPrice= $totalPrice + ($roomcostArray['price'] * ($days + 1));
            }
        }

        $result= array("duration"=> "Total duration of accomodation is " .$days . " Days, " . $hours .  " Hours, " . $minutes  . " Minutes!", "amount"=> $totalPrice, "bookingdata"=>$jsonArray, "checkout"=>$checkoutTime, "checkoutInMillis"=>$end, "tax"=>$taxAmount);

        $confirmedPayemt["paymentData"]= (object)$result;


       // print_r(json_encode($confirmedPayemt));
        $this->load->view('templates/header');
        $this->load->view('pages/checkoutdone', $confirmedPayemt);
        $this->load->view('templates/footer');
    }

    public function checkoutRoomForUser()
    {
        $data = $this->input->post('data');
        $transactionId= $data['id'];
        $timeDuration= $data['duration'];
        $checkinDatePeriod= $data['checkinDate'];
        $checkoutDatePeriod= $data['initialcheckoutdate'];
        $taxamount= $data['taxableAmount'];
        
        $freeRooms= $data['room'];
        $roomList= explode(',', $freeRooms);
        for($x= 0; $x< sizeof($roomList); $x++)
        {
            $this->GetAllPendingTransactionsModel->freeRoom($checkinDatePeriod, $checkoutDatePeriod, $roomList[$x]);
        }

        $this->GetAllPendingTransactionsModel->updateTaxForId($transactionId, $taxamount);

        echo $this->GetAllPendingTransactionsModel->checkoutRoomForUserModel($transactionId, $timeDuration);
    }
}
?>
