<?php
defined('BASEPATH') or exit('No direct script access allowed');

class GenerateInvoiceController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('CompletedTransactionsModel');
    }

    public function index()
    {

        if (!$this->session->userdata('logged_in')) {
            redirect('LoginController/logout');
        }

        $transactionId = $this->input->get('id');
        // print_r(json_encode($this->CompletedTransactionsModel->getCompletedTransactionsForId($transactionId)));
        $data['roomprice'] = $this->CompletedTransactionsModel->getAllPriceOfRoom();
        $data['user'] = $this->CompletedTransactionsModel->getCompletedTransactionsForId($transactionId);
        // var_dump($data);
        $this->load->helper('form');
        $this->load->view('pages/invoice', $data);
    }
}
