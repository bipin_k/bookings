<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class GetAllPendingTransactionsController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('GetAllPendingTransactionsModel');
    }

    public function index()
    {

        if (!$this->session->userdata('logged_in')) {
            redirect('LoginController/logout');
        }

        $this->load->model('GetAllPendingTransactionsModel');
        $data = $this->GetAllPendingTransactionsModel->getAllPendingTransactionData();
        // print_r($data);
        $this->load->helper('form');
        $this->load->view('templates/header');
        $this->load->view('pages/PendingTransactions', array(
            'data' => $data,
        ));
        $this->load->view('templates/footer');
    }

    public function getTransactionId()
    {
        $transactionId = $this->input->get('id');
        $this->load->model('GetAllPendingTransactionsModel');
        header('Content-type: application/json');
        echo json_encode($this->GetAllPendingTransactionsModel->getDataForTransactionId($transactionId));
    }

    public function getPriceOfRooms()
    {
        $transactionId = $this->input->get('id');
        $this->load->model('GetAllPendingTransactionsModel');
        header('Content-type: application/json');
        $rooms = json_encode($this->GetAllPendingTransactionsModel->getRoomPrice($transactionId));
        $jsonArray = json_decode($rooms, true);
        $firstName = $jsonArray['bookedrooms'];

        $checkinTime = $jsonArray['checkindate'] . ' ' . $jsonArray['checkintime'];
        $start = strtotime($checkinTime);

        print_r("\n");
        date_default_timezone_set("Asia/Calcutta");
        $checkoutTime = date('Y-m-d H:i:s');
        $end = strtotime($checkoutTime);
        $date = date_create_from_format('Y-m-d H:i:s', $checkinTime);
        date_default_timezone_set("Asia/Calcutta");
        $start = $date->getTimestamp() * 1000;
        $end = $end * 1000;
        $diff = $end - $start;

        $days = floor($diff / (60 * 60 * 1000 * 24));
        $hours = floor(($diff / (60 * 60 * 1000)) - ($days * 24));
        $minutes = floor(($diff / (1000 * 60)) - ($days * 24 * 60) - ($hours * 60));
        // echo 'Total duration of accomodation: ' .$days . ' days, ' . $hours .  ' hours, ' . $minutes  . ' minutes';

        $myArray = explode(',', $firstName);
        $totalPrice = 0;
        for ($x = 0; $x < sizeof($myArray); $x++) {
            $roomcost = json_encode($this->GetAllPendingTransactionsModel->getPriceOfEachRoom($myArray[$x]));
            $roomcostArray = json_decode($roomcost, true);

            if ($roomcostArray['price'] > 999) {
                $totalPrice = $totalPrice + (($roomcostArray['price'] * ($days + 1)) * 1.12);
            } else {
                $totalPrice = $totalPrice + ($roomcostArray['price'] * ($days + 1));
            }
        }

        $result = array("duration" => "Total duration of accomodation is " . $days . " Days, " . $hours . " Hours, " . $minutes . " Minutes!", "amount" => $totalPrice, "bookingdata" => $jsonArray, "checkout" => $checkoutTime, "checkoutInMillis" => $end);
        echo json_encode($result);
        // print_r(json_encode($result));
    }

    public function getCheckoutData()
    {
        $final_result = array();
        $transactionId = $this->input->get('id');
        $final_result["transaction"] = $this->GetAllPendingTransactionsModel->getDataForTransactionId($transactionId);
        // array_push($final_result, $this->GetAllPendingTransactionsModel->getDataForTransactionId($transactionId));

        $rooms = json_encode($this->GetAllPendingTransactionsModel->getRoomPrice($transactionId));
        $jsonArray = json_decode($rooms, true);
        $firstName = $jsonArray['bookedrooms'];

        $checkinTime = $jsonArray['checkindate'] . ' ' . $jsonArray['checkintime'];
        $start = strtotime($checkinTime);

        // print_r("\n");
        date_default_timezone_set("Asia/Calcutta");
        $checkoutTime = date('Y-m-d H:i:s');
        $end = strtotime($checkoutTime);
        $date = date_create_from_format('Y-m-d H:i:s', $checkinTime);
        date_default_timezone_set("Asia/Calcutta");
        $start = $date->getTimestamp() * 1000;
        $end = $end * 1000;
        $diff = $end - $start;

        $days = floor($diff / (60 * 60 * 1000 * 24));
        $hours = floor(($diff / (60 * 60 * 1000)) - ($days * 24));
        $minutes = floor(($diff / (1000 * 60)) - ($days * 24 * 60) - ($hours * 60));
        // echo 'Total duration of accomodation: ' .$days . ' days, ' . $hours .  ' hours, ' . $minutes  . ' minutes';

        $myArray = explode(',', $firstName);
        $totalPrice = 0;
        for ($x = 0; $x < sizeof($myArray); $x++) {
            $roomcost = json_encode($this->GetAllPendingTransactionsModel->getPriceOfEachRoom($myArray[$x]));
            $roomcostArray = json_decode($roomcost, true);

            if ($roomcostArray['price'] > 999) {
                $totalPrice = $totalPrice + (($roomcostArray['price'] * ($days + 1)) * 1.12);
            } else {
                $totalPrice = $totalPrice + ($roomcostArray['price'] * ($days + 1));
            }
        }

        $result = array("daysCount" => $days + 1, "duration" => "Total duration of accomodation is " . $days . " Days, " . $hours . " Hours, " . $minutes . " Minutes!", "amount" => $totalPrice, "bookingdata" => $jsonArray, "checkout" => $checkoutTime, "checkoutInMillis" => $end);

        $final_result["final_amount"] = (object) $result;

        $this->session->set_userdata('finalAmount', $totalPrice);

        // array_push($final_result, $result);

        // print_r($final_result);

        // header('Content-type: application/json');
        // echo json_encode($final_result);

        // print_r(json_encode($final_result));
        $this->load->view('templates/header');
        // print_r(json_encode($final_result));
        $this->load->view('pages/checkoutpage', $final_result);
        //
        $this->load->view('templates/footer');
    }

    public function submitdiscount()
    {
        $transactionId = $this->input->post('id');
        $discountamount = $this->input->post('discount');
        $pendingamount = $this->input->post('pending');
        $countOfDays = $this->input->post('daysNumber');

        $checkoutDate = $this->input->post('checkoutdate');
        $checkoutTime = $this->input->post('checkouttime');

        $this->GetAllPendingTransactionsModel->updateCheckoutData($transactionId, $checkoutDate, $checkoutTime, $countOfDays);

        if ($discountamount == null) {
            $discountamount = 0;
        }
        echo $this->GetAllPendingTransactionsModel->updatediscount($transactionId, $discountamount, $pendingamount);
    }
}
