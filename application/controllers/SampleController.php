<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SampleController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->helper('form');
        $this->load->view('templates/header');
        $this->load->view('pages/sample');
        $this->load->view('templates/footer');
    }
}