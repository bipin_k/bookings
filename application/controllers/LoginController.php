<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LoginController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('usermodel');
    }

    public function index()
    {
        $this->load->helper('form');

        $this->load->view('pages/login');
        $this->load->view('templates/footer');
    }

    public function logout()
    {
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('LoginController', 'refresh');
    }

    public function loginSuccess()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        /**
         * Checking whether the entered Email is present or not
         *
         * @var [Boolean] True is the user is present false otherwise
         */

        $emailVerify = $this->usermodel->checkEmail($email);
        if (!$emailVerify) {
            $this->session->set_flashdata('msg', 'invalidentries');
            redirect('LoginController', 'refresh');
            return;
        }

        $result = $this->usermodel->login($email, $password);

        if (count($result[0]) > 0) {
            //Valid Password

            $row = $result[0];

            $sess_array = encrypt_array($row);

            $this->session->set_userdata('logged_in', $sess_array);

            redirect('CheckAvailabilityController', 'refresh');
        } else {
            $this->session->set_flashdata('msg', 'invalidentries');
            redirect('LoginController', 'refresh');
            return;
        }
    }
}
