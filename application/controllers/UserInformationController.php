<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserInformationController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        if (!$this->session->userdata('logged_in')) {
            redirect('LoginController/logout');
        }

        $this->load->helper('form');
        $this->load->view('templates/header');
        $this->load->view('pages/userInformation');
        $this->load->view('templates/footer');
    }

    public function loginSuccess()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        echo $email;
        echo "\n";
        echo $password;
    }
}
