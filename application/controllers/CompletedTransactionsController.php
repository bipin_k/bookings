<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CompletedTransactionsController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('CompletedTransactionsModel');
    }

    public function index()
    {

        if (!$this->session->userdata('logged_in')) {
            redirect('LoginController/logout');
        }

        $data = (object) $this->CompletedTransactionsModel->getCompletedTransactions();
        // print_r($data);
        $this->load->helper('form');
        $this->load->view('templates/header');
        $this->load->view('pages/completedTransactions', array(
            'data' => $data,
        ));
        $this->load->view('templates/footer');
    }

    public function getCompletedTransactionsForId()
    {
        $transactionId = $this->input->get('id');
        header('Content-Type: application/json; charset=UTF-8');
        print_r(json_encode($this->CompletedTransactionsModel->getCompletedTransactionsForId($transactionId)));
    }
}
