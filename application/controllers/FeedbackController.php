<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FeedbackController extends CI_Controller
{
	public function getFeedback()	
	{
		$this->load->model('GetFeedbackModel');
		header('Content-type: application/json');
		echo json_encode($this->GetFeedbackModel->getAllData());
	}

	public function showerror()
	{
		$this->output->set_content_type('application/json');
		$this->output->set_status_header(401);
		echo json_encode(array('pls'=>0, 'msg' => "No data"));
		// $this->output->set_status_header([$code = 200[, $text = '']]);
	}
}
?>